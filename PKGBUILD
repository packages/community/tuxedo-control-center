# Maintainer: Mark Wagie <mark at manjaro dot org>

pkgname=tuxedo-control-center
pkgver=2.1.16
pkgrel=1
_nodeversion=18
pkgdesc="A tool to help you control performance, energy, fan and comfort settings on TUXEDO laptops."
arch=('x86_64')
url="https://github.com/tuxedocomputers/tuxedo-control-center"
license=('GPL-3.0-or-later')
depends=(
  'alsa-lib'
  'gtk3'
  'libxss'
  'nss'
  'python'
  'systemd'
  'tuxedo-drivers-dkms'
)
makedepends=(
  'git'
  'libxcrypt-compat'
  'nvm'
  'python'
)
optdepends=(
  'libayatana-appindicator: tray icon'
)
options=('!strip')
install="${pkgname}.install"
source=("${pkgname}-${pkgver}.tar.gz::$url/archive/refs/tags/v${pkgver}.tar.gz")
sha256sums=('e48350f151d2c5c1ce64bb0c54551de4e3aae0e0d3e958f1b1c5799801fc9622')

_ensure_local_nvm() {
  # let's be sure we are starting clean
  which nvm >/dev/null 2>&1 && nvm deactivate && nvm unload
  export NVM_DIR="$srcdir/.nvm"

  # The init script returns 3 if version specified
  # in ./.nvrc is not (yet) installed in $NVM_DIR
  # but nvm itself still gets loaded ok
  source /usr/share/nvm/init-nvm.sh || [[ $? != 1 ]]
}

prepare() {
  cd "${pkgname}-${pkgver}"
  _ensure_local_nvm
  nvm install "${_nodeversion}"

  # Bump node version
  sed -i "s/node14/node${_nodeversion}/g" package.json
}

build() {
  cd "${pkgname}-${pkgver}"
  export npm_config_cache="${srcdir}/npm_cache"
  export NODE_OPTIONS=--openssl-legacy-provider  # only required for Node.js >=17
  _ensure_local_nvm
  npm install
  npm run build
  npm run pack-prod deb
}

package() {
  cd "${pkgname}-${pkgver}"
  _dist_data="${pkgdir}/opt/${pkgname}/resources/dist/${pkgname}/data/dist-data"

  install -d "${pkgdir}"/{opt/"${pkgname}",usr/bin}
  cp -av dist/packages/linux-unpacked/* "${pkgdir}/opt/${pkgname}/"
  chmod 4755 "${pkgdir}/opt/${pkgname}/chrome-sandbox"
  ln -sv "/opt/${pkgname}/${pkgname}" "${pkgdir}/usr/bin/"

  install -Dm644 "${_dist_data}/${pkgname}.desktop" -t \
    "${pkgdir}/usr/share/applications/"

#  install -Dm644 "${_dist_data}/${pkgname}-tray.desktop" -t \
#    "${pkgdir}/etc/skel/.config/autostart/"

  install -Dm644 "${_dist_data}"/com.tuxedocomputers.{tccd,tomte}.policy -t \
    "${pkgdir}/usr/share/polkit-1/actions/"

  install -Dm644 "${_dist_data}/com.tuxedocomputers.tccd.conf" -t \
    "${pkgdir}/usr/share/dbus-1/system.d/"

  install -Dm644 "${_dist_data}/tccd.service" -t \
    "${pkgdir}/usr/lib/systemd/system/"
  install -Dm644 "${_dist_data}/tccd-sleep.service" -t \
    "${pkgdir}/usr/lib/systemd/system/"

  install -Dm644 "${_dist_data}/99-webcam.rules" -t "$pkgdir/usr/lib/udev/rules.d/"

  install -Dm644 "dist/${pkgname}/data/dist-data/com.tuxedocomputers.tcc.metainfo.xml" -t \
    "${pkgdir}/usr/share/metainfo/"

  install -Dm644 "${_dist_data}/${pkgname}_256.svg" \
    "${pkgdir}/usr/share/icons/hicolor/scalable/apps/${pkgname}.svg"
}
